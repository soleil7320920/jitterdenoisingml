#!/usr/bin/env python
# coding: utf-8

# # Denoise 2D Maps

# ## Useful import

# In[1]:


import tensorflow as tf
from tensorflow import keras
from keras import layers
from keras.models import Model
import matplotlib.pyplot as plt
import numpy as np


# ## Useful functions

# In[5]:


def display(array1, array2):
    """Displays ten random images from each array."""
    if len(array1) >= 10:
        n = 10
    else:
        n = len(array1)
    indices = np.random.randint(len(array1), size=n)
    images1 = array1[indices, :]
    images2 = array2[indices, :]

    plt.figure(figsize=(20, 4))
    for i, (image1, image2) in enumerate(zip(images1, images2)):
        ax = plt.subplot(2, n, i + 1)
        plt.imshow(image1.reshape(N, N))
        plt.viridis()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        ax = plt.subplot(2, n, i + 1 + n)
        plt.imshow(image2.reshape(N, N))
        plt.viridis()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

    plt.show()

def f(x, y, A, x0, y0, sigma_x, sigma_y, theta):
    """Rotated Gaussian"""
    theta = np.radians(theta)
    sigx2 = sigma_x**2; sigy2 = sigma_y**2
    a = np.cos(theta)**2/(2*sigx2) + np.sin(theta)**2/(2*sigy2)
    b = np.sin(theta)**2/(2*sigx2) + np.cos(theta)**2/(2*sigy2)
    c = np.sin(2*theta)/(4*sigx2) - np.sin(2*theta)/(4*sigy2)
    
    expo = -a*(x-x0)**2 - b*(y-y0)**2 - 2*c*(x-x0)*(y-y0)
    return A*np.exp(expo)


# ## Construct Dataset

# In[6]:


def simul_XES(N, loop, plotting=False):
    data = np.zeros((loop,N,N))
    noisy_data = np.zeros((loop,N,N))
    x0_distrib = np.random.normal(3,0.25,loop)
    y0_distrib = np.random.normal(2,0.25,loop)
    sigx_distrib = np.random.normal(1.5,0.25,loop)
    sigy_distrib = np.random.normal(0.75,0.125,loop)
    for l in range(loop):
        x = np.linspace(0,5,N)	
        y = np.linspace(0,5,N)
        theta = 45  # deg
        #x0 = 3 + (np.random.rand(1)-0.5)*2
        #y0 = 2 + (np.random.rand(1)-0.5)*2
        #sigx = 1.5
        #sigy = 0.75
        x0 = x0_distrib[l]
        y0 = y0_distrib[l]
        sigx = sigx_distrib[l]
        sigy = sigy_distrib[l]        
        A = 2	
        Xg, Yg = np.meshgrid(x, y)
        Z = f(Xg, Yg, A, x0, y0, sigx, sigy, 45)
        #Zel = f(Xg, Yg, A, 0, 1, 20, 0.2, 45)
        Zel = f(Xg, Yg, A, 0, 0, 20, 0.2, 0)
        elastic_weight = 0.7 * np.random.rand(1)
        data[l] = Z + Zel * elastic_weight
        if plotting: 
            plt.figure(1)
            plt.clf()
            plt.pcolormesh(x,y,data[l])
            plt.axis('square')
        #jitter = (np.random.rand(N)-0.5)/4
        jitter = np.random.normal(0,0.125,N)
        x2 = x+jitter
        for i in range(N):
            noisy_data[l][i,:] = np.interp(x,x2,Z[i,:]+Zel[i,:] * elastic_weight)
        if plotting :
            plt.figure(2)
            plt.pcolormesh(x,y,noisy_data[l])
            plt.axis('square')
            plt.show()
    return data,noisy_data


# In[15]:


#Dataset size
loop = 1200
#Image size
N = 40
x,y = simul_XES(N,loop,plotting=False)


# ## Create Train / Test datasets

# In[17]:


p = 5/6
tmp = np.split(x,[int(loop*p)])
x_train = tmp[0]
x_test = tmp[1]
tmp = np.split(y,[int(loop*p)])
y_train = tmp[0]
y_test = tmp[1]

x_train = x_train.reshape(-1,N,N,1)
x_test = x_test.reshape(-1,N,N,1)
y_test = y_test.reshape(-1,N,N,1)
y_train = y_train.reshape(-1,N,N,1)

xmax = x_train.max()
x_train = x_train / xmax
x_test= x_test / xmax


# In[18]:


display(x_train,y_train)


# ## Construct AutoEncoder Model
# from https://keras.io/examples/vision/autoencoder/

# In[12]:


input = layers.Input(shape=(N, N, 1))

# Encoder
x = layers.Conv2D(32, (3, 3), activation="relu", padding="same")(input)
x = layers.MaxPooling2D((2, 2), padding="same")(x)
x = layers.Conv2D(32, (3, 3), activation="relu", padding="same")(x)
x = layers.MaxPooling2D((2, 2), padding="same")(x)

# Decoder
x = layers.Conv2DTranspose(32, (3, 3), strides=2, activation="relu", padding="same")(x)
x = layers.Conv2DTranspose(32, (3, 3), strides=2, activation="relu", padding="same")(x)
x = layers.Conv2D(1, (3, 3), activation="sigmoid", padding="same")(x)

# Autoencoder
autoencoder = Model(input, x)
autoencoder.compile(optimizer="adam", loss="binary_crossentropy")
autoencoder.summary()


# ## Autotrain

# In[19]:


autoencoder.fit(
    x=x_train,
    y=x_train,
    epochs=50,
    batch_size=128,
    shuffle=True,
    validation_data=(x_test, x_test),
)


# In[20]:


predictions = autoencoder.predict(x_test)
display(x_test, predictions)


# ## Train with noisy data

# In[21]:


autoencoder.fit(
    x=y_train,
    y=x_train,
    epochs=100,
    batch_size=128,
    shuffle=True,
    validation_data=(y_test, x_test),
)


# In[22]:


predictions = autoencoder.predict(y_test)
display(y_test, predictions)


# In[25]:


from PIL import Image
im_frame = Image.open('map_gray_cropped.png')
print(im_frame.size)
im_small = im_frame.resize((40,40))
np_small = np.array(im_small)[:,:,0]
np_small = np_small.reshape(-1,N,N,1)
np_small = np.flipud(np_small) / np_small.max()


# In[26]:


predictions = autoencoder.predict(np_small)
display(np_small, predictions)


# In[366]:


np_small.shape


# In[ ]:




